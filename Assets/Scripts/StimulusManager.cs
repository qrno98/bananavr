using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StimulusManager
{
    public List<StimulusSO> _possibleStimuli;

    Block[] _blocks;
    public Queue<StimulusSO>[] blockQueues;

    //     Completions that are both compatible with structure and knowledge
    //     "banana", "A1" // normal long banana
    
    //     and completions that are incompatible with structure and knowledge
    //     "banana", "A2" // two tiny bananas
    
    //     Completions that are compatible with structure, but incompatible with knowledge
    //     "apple", "B1" // elongated apple
    
    //     and completions that are incompatible with structure, but compatible with knowledge
    //     "apple", "B2 // two normal apples
    public StimulusManager(List<StimulusSO> uniqueStimuli){
        
        blockQueues = new Queue<StimulusSO>[]{
            GenerateBlock(uniqueStimuli),
            GenerateBlock(uniqueStimuli),
            GenerateBlock(uniqueStimuli),
            GenerateBlock(uniqueStimuli)
        };
    }

    Queue<StimulusSO> GenerateBlock(List<StimulusSO> uniqueStimuli, 
        int duplicationQuantity = 2){
        
        List<StimulusSO> normalStims = uniqueStimuli.ConvertAll(stim => {
            StimulusSO tempClone = stim.Clone<StimulusSO>();
            tempClone._isMirrored = false;
            return tempClone;
        });
        List<StimulusSO> mirrorStims = uniqueStimuli.ConvertAll(stim => {
            StimulusSO tempClone = stim.Clone<StimulusSO>();
            tempClone._isMirrored = true;
            return tempClone;
        });

        _possibleStimuli = new List<StimulusSO>();
        _possibleStimuli.AddRange(normalStims);
        _possibleStimuli.AddRange(mirrorStims);

        // fixation cross, occlusion, stimulus
        return new Block(_possibleStimuli, duplicationQuantity, true).GetQueue();        
    }

    public void DebugPrint(){
    // foreach(var block in blocks){
        int counter = 1;
        foreach(var stimulus in _possibleStimuli){
            Debug.Log(
                counter
                + ": "
                + stimulus._conditionType
                + " "
                + stimulus._isMirrored
                + " "
                + stimulus._uniqueName
            );
            counter++;
        }

        counter = 1;
        foreach(var stimulus in blockQueues[0]){
            Debug.Log(
                counter
                + ": "
                + stimulus._conditionType
                + " "
                + stimulus._isMirrored
                + " "
                + stimulus._uniqueName
            );
            counter++;
        }            
    }
}
