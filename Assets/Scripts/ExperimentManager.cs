using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using Valve.VR;


public class ExperimentManager : MonoBehaviour
{   
    private StimulusManager _stimManager;
    
    private GameObject _currentStimulus;
    
    // stimulus spawn position (empty node on table surface)
    [SerializeField] 
    public Transform stimSpawnPosition;

    [SerializeField] 
    private GameObject box;

    [SerializeField] 
    private GameObject breakPanel;

    [SerializeField] 
    private GameObject whitePanel;

    [SerializeField] 
    private GameObject blackPanel;
    [SerializeField] 
    private GameObject finishedPanel;

    private string _currentState = "initial-startscreen";

    private Queue<StimulusSO> _currentBlockQueue;
    private int _currentBlockQueueIndex = 0;

    private float _clock = 0f; // passed time in a particular state in secs
    
    private float STIM_TIME = 1.0f; // revealed stimulus is shown for 1.0f seconds
    private float ISI_TIME = 1.5f; // interstimulus: black screen is shown for 1.5f seconds

    string GetCurrentState(){
        return _currentState;
    }

    private int frames = 0; // used for debugging

    void Start(){
        StimulusSO[] uniqueStimuli = LoadStimuliScriptableObjectsFromResources();        
        DebugLogAllUniqueStimuli(uniqueStimuli);

        _stimManager = new StimulusManager(new List<StimulusSO>(uniqueStimuli));
        // _stimManager.DebugPrint();
        
        blackPanel.SetActive(false);
        breakPanel.SetActive(false);
        finishedPanel.SetActive(false);
        whitePanel.SetActive(true);
    }

    // TODO determine sequence type (cross, occl, stim vs. cross, stim, occl) in
    // block class or rather here hardcoded in update class?
    // FOCUSING ON OCCLUSION THEN STIIM (at least for now)

    // Each trial began with the presentation of a fixation cross which remained 
    // on the screen for 1000 ms. 
    // Afterwards an occlusion stimulus and one of its two completions 
    // were shown sequentially for 1000 ms. 
    // Between each trial a blank screen was presented for 1500 ms. 
    
    // In half of the trials the occlusion stimulus was presented first and ERPs 
    // were recorded when the partly occluded shape was disoccluded, 
    // revealing the whole shape (Fig. 5A). 
    
    // In the other half of the trials (presented in separate blocks) a fully visible shape was 
    // presented first and ERPs were recorded when the shape was partly occluded (Fig. 5B).

    // no fixation cross
    // start screen / pause screen
    // idle standing in room. Button press -> start sequence
    // 1000ms occluded stimulus (display box, dequeue stim & instantiate)
    // 1000ms one of two stimulus completions (hide box)
    // fade to black then back to idle (display box, dequeue stim & instantiate)

    // variant B
    // start screen / pause screen
    // idle standing in room. Button press -> start sequence
    // 1000ms one of two stimulus completions (hide box, dequeue stim & instantiate)
    // 1000ms occluded stimulus (display box)
    // fade to black then back to idle

    void Update()
    {
        Debug.Log((SteamVR_Actions._default.BoxTrigger.state || Input.GetKeyDown(KeyCode.Space)));
        /*
        if (SteamVR_Input._default.inActions.BoxTrigger.GetStateDown(SteamVR_Input_Sources.Any))
        {
            Debug.Log("Key Pressed");
        }
        */
        if (_currentState == "initial-startscreen" && (SteamVR_Actions._default.BoxTrigger.state || Input.GetKeyDown(KeyCode.Space)))
        {
            _currentState = "idle";

            _currentBlockQueue = _stimManager.blockQueues[_currentBlockQueueIndex];

            DisplayStimulusAndBox();

            whitePanel.SetActive(false);            
        }
        else if(_currentState == "idle" && (SteamVR_Actions._default.BoxTrigger.state || Input.GetKeyDown(KeyCode.Space)))
        {                        
            // TODO animate box
            box.SetActive(false);

            _clock = 0;
            _currentState = "show-stimulus";
        }
        else if(_currentState == "show-stimulus"){
            
            _clock += Time.deltaTime;

            if(_clock >= STIM_TIME){ // in secs
                _clock = 0;
                _currentState = "interstimulus-interval";
                blackPanel.SetActive(true);
                Destroy(_currentStimulus);
            }
        }
        else if(_currentState == "interstimulus-interval"){

            _clock += Time.deltaTime;

            if(_clock >= ISI_TIME){
                _clock = 0;
                blackPanel.SetActive(false);

                if(_currentBlockQueue.Count > 0){
                    _currentState = "idle";
                    DisplayStimulusAndBox();
                }
                else {
                    _currentBlockQueueIndex++;
                    if(_currentBlockQueueIndex >= _stimManager.blockQueues.Length){
                        finishedPanel.SetActive(true);                        
                    }
                    else{
                        SetStateToBlockBrake();
                    }
                }                
            }
        }
        else if(_currentState == "block-break" && (SteamVR_Actions._default.BoxTrigger.state || Input.GetKeyDown(KeyCode.Space)))
        {
            _currentState = "idle";

            
            _currentBlockQueue = _stimManager.blockQueues[_currentBlockQueueIndex];
            
            breakPanel.SetActive(false);

            DisplayStimulusAndBox();            
        }

        // DEBUGGING
        // frames++;
        // if (frames % 10 == 0) { //If the remainder of the current frame divided by 10 is 0 run the function.
        //     Debug.Log(_currentState);
        // }
    }

    void DisplayStimulusAndBox(){
        
        box.SetActive(true);
        
        StimulusSO tempStimSO = _currentBlockQueue.Dequeue();
        _currentStimulus = (GameObject)Instantiate(tempStimSO.model, stimSpawnPosition);
        
        if(tempStimSO._isMirrored == true){
            // TODO use correct axis for rotation once proper models are implemented
            _currentStimulus.transform.Rotate(new Vector3(0, 45, 0));            
        }        
    }

    void SetStateToBlockBrake(){
        _currentState = "block-break";        
        _currentBlockQueue = _stimManager.blockQueues[_currentBlockQueueIndex];
                
        breakPanel.SetActive(true);
    }

    StimulusSO[] LoadStimuliScriptableObjectsFromResources(){
        // must be placed within Assets\Resources folder
        string stimulusSOPath = "stimulus_scriptable_objects";
        object[] temp = Resources.LoadAll(stimulusSOPath, typeof(StimulusSO));
        
        return temp.Cast<StimulusSO>().ToArray();
    }

    void DebugLogAllUniqueStimuli(StimulusSO[] uniqueStimuli){

        string logNames = "Logging all scriptable objects: \n";        
        foreach (var stimSO in uniqueStimuli)
        {
            logNames += string.Join(
                ", ", 
                stimSO._uniqueName,
                stimSO._conditionType,
                stimSO._isMirrored
            );
            logNames += "\n";
        }
        Debug.Log(logNames);
    }
}
