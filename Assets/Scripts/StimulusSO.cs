using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Stimulus", menuName = "Stimulus")]
public class StimulusSO : ScriptableObject
{

    public GameObject model;

    public string _uniqueName;
    public string _conditionType;
    public bool _isMirrored;
}
