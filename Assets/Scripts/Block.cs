using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;

public class Block
{   
    Queue<StimulusSO> _stimuli = new Queue<StimulusSO>();

    public Block(List<StimulusSO> possibleStimuli, 
        int duplicationQuantity = 2, bool includeMirrored = true){

        for(int i = 0; i < duplicationQuantity; i++){

            var rnd = new System.Random();
            var randomizedList = possibleStimuli.OrderBy(item => rnd.Next()).ToList();

            foreach (StimulusSO tempStim in randomizedList)
            {        
                _stimuli.Enqueue(tempStim);
            }            
        }
    }

    public Queue<StimulusSO> GetQueue(){
        return _stimuli;
    }
}
